# JCVI-syn3A

![JCVI-syn3A](./assets/JCVI-syn3A.png)
JCVI-syn3A protein coding genes (A) and proteomics (B)distributed to four functional classes: metabolism (brown), genetic informationprocessing (blue), cellular processes such as cell division (green), and unclearfunction(gray). Shading within the four functional classes indicate subsystemswithin the class, such as nucleotide metabolism in metabolism or transcriptionin genetic information processing. NCBI GenBank CP016816.2: https://www.ncbi.nlm.nih.gov/nuccore/CP016816.2

JCVI-syn3A is a minimal bacterial cell with a 543 kbp genome consisting of 493 genes. Of the 452 protein-coding genes, 143 are assigned to metabolism and 212 are assigned to genetic information processing. Using genome-wide proteomics and experimentally measured kinetic parameters from the literature kinetic models are designed for the genetic information processes of DNA replication, replication initiation, transcription, and translation which are solved stochastically and averaged over 1,000 replicates/cells. The model predicts the time required for replication initiation and DNA replication to be 8 and 50 min on average respectively and the number of proteins and ribosomal components to be approximately doubled in a cell cycle. The model of genetic information processing when combined with the essential metabolic and cell growth networks will provide a powerful platform for studying the fundamental principles of life.

## Lattice Microbes

Spatial stochastic simulation is a valuable technique for studying reactions in biological systems. With the availability of high-performance computing (HPC), the method is poised to allow integration of data from structural, single-molecule and biochemical studies into coherent computational models of cells. Lattice Microbes is the software package for simulating such cell models on HPC systems. The software performs either well-stirred or spatially resolved stochastic simulations with approximated cytoplasmic crowding in a fast and efficient manner. The algorithm efficiently samples the reaction-diffusion master equation using NVIDIA graphics processing units. Display of cell models and animation of reaction trajectories involving millions of molecules is facilitated using a plug-in to the popular VMD visualization platform. The Lattice Microbes software is open source and available for download at http://www.scs.illinois.edu/schulten/lm

Please follow the instructions in https://docs.docker.com/engine/install for the docker installation. Run the below command for interactive notebooks
```
docker-compose pull
docker-compose up
```