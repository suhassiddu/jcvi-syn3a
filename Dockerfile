FROM python:2.7

WORKDIR /deps
COPY requirements.txt /deps/

RUN python -m pip install --upgrade pip && \
    pip install -r requirements.txt

RUN wget faculty.scs.illinois.edu/schulten/lm/download/lm23/lm-2.3.0-linux-el7.tar.gz && \
    tar zxvf lm-2.3.0-linux-el7.tar.gz && \
    cd lm-2.3.0 && \
    cp bin/* /usr/local/bin && \
    mkdir -p /usr/local/lib/lm && \
    cp lib/lm/* /usr/local/lib/lm && \
    cp -r lib/python /usr/local/lib && \
    rm -rf /deps/*

ENV PYTHONPATH $PYTHONPATH:/usr/local/lib/python:/usr/local/lib/lm